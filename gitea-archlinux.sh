USERNAME=deployer

pacman -Syu --noconfirm
pacman -S --noconfirm \
  git \
  base-devel \
  zsh

useradd -m -g users -s /usr/bin/zsh $USERNAME
groupadd sudo
usermod -aG sudo $USERNAME
echo "$USERNAME:$PASSWORD" | chpasswd
sed -i '/%sudo/s/^# //' /etc/sudoers
mkdir -p /home/$USERNAME/.ssh
cp /root/.ssh/authorized_keys >> /home/$USERNAME/.ssh/authorized_keys
chown -R $USERNAME /home/$USERNAME/.ssh

# install yay
cd /opt
git clone https://aur.archlinux.org/yay.git
cd yay
chown -R $USERNAME ./

su - $USERNAME << EOF
  cd /opt/yay && makepkg --noconfirm -Ssi
  git clone --recursive https://github.com/sorin-ionescu/prezto.git \$HOME/.zprezto
  setopt EXTENDED_GLOB
  for rcfile in \$HOME/.zprezto/runcoms/^README.md(.N); do
    ln -s "\$rcfile" \$HOME/.\${rcfile:t}
  done
EOF