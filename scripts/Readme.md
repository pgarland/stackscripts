## Links
> https://wiki.archlinux.org/index.php/Ruby_on_Rails#Puma_.28with_Nginx_as_reverse_proxy_server.29
> http://sirupsen.com/what-I-wish-a-ruby-programmer-had-told-me-one-year-ago/

## Ruby Scripting Commands:
return output ``` `pwd` ```

run command, replacing current process: `exec('pwd')`

run command, return success boolean `system('pwd')`

run command, return output as io object `IO.popen('pwd')`

return array of stdin, stdout, and stderr streams io objects:
```
#!ruby
require 'open3'

Open3.popen3('pwd')
```

file utilities
```
#!ruby
require 'fileutils'

FileUtils.cd(dir, options) {|dir| .... }
FileUtils.pwd()
FileUtils.mkdir(dir, options)
```