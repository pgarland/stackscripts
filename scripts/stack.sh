#!/bin/bash

HOSTNAME="linode"
USERNAME="deployer"
USERPASS="bearcreek"
GITNAME="Peter Garland"
GITEMAIL="pngarland@gmail.com"
APPNAME="my_rails_app"
HOME="/home/$USERNAME"

function run-basic-configuration() {
  # set hostname
  echo $HOSTNAME > /etc/hostname
  hostname -F /etc/hostname

  ## Set base locale
  LOCALE_BASE=$(echo $LOCALE | cut -d. -f1)

  ## Get a sensible locale (in case the selected one doesn't exist)
  grep "^${LOCALE}" /etc/locale.gen &>/dev/null
  (( $? > 0 )) && LOCALE=${LOCALE_BASE}
  grep "^${LOCALE}" /etc/locale.gen &>/dev/null
  if (( $? > 0 ))
  then
    LOCALE="en_US.UTF-8"
    LOCALE_BASE="en_US"
  fi
  echo "LANG=$LOCALE" > /etc/locale.conf

  ## Set timezone to sensible value (in case the selected one doesn't exist)
  grep "${TIMEZONE}" /usr/share/zoneinfo/zone.tab &>/dev/null
  (( $? > 0 )) && TIMEZONE="America/Chicago"

  # user config
  echo "%wheel ALL=(ALL) ALL" >> /etc/sudoers
}

function install-basic-packages() {
  pacman -Syu --noconfirm
  pacman -S --noconfirm base-devel git nginx postgresql
}

function create-vm-user() {
useradd -m -g users -G wheel -s /bin/bash $1
passwd $1 <<EOF
$USERPASS
$USERPASS
EOF
}

function as-user() {
  su - $USERNAME -c "$@"
}

function as-postgres-user() {
  su - postgres -c "$@"
}

function install-ruby() {
  as-user "git clone https://github.com/sstephenson/rbenv.git ~/.rbenv"
  as-user "git clone https://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build"
  mkdir $HOME/.bundle
  echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> $HOME/.bash_profile
  echo 'eval "$(rbenv init -)"' >> $HOME/.bash_profile
  as-user "source $HOME/.bash_profile; rbenv install 2.2.3"
  as-user "source $HOME/.bash_profile; rbenv global 2.2.3"
}

function setup-git() {
  as-user "ssh-keygen -t rsa -f $HOME/.ssh/id_rsa -N $USERPASS"
  as-user "git config --global user.name $GITNAME"
  as-user "git config --global user.email $GITEMAIL"
}

function setup-postgres() {
  chown -R postgres /var/lib/postgres/
  as-postgres-user "initdb -D '/var/lib/postgres/data'"
  chown -R postgres /run/postgresql/
  systemctl enable postgresql
  systemctl start postgresql
}

function create-postgres-user() {
  as-postgres-user "psql -c \"CREATE ROLE $USERNAME WITH PASSWORD '$USERPASS' SUPERUSER CREATEDB CREATEROLE INHERIT LOGIN;\""
  as-postgres-user "createdb $USERNAME"
}

function setup-nginx() {
  cp /etc/nginx/nginx.conf /etc/nginx/nginx.conf.backup
  systemctl enable nginx
  systemctl start nginx
}

function setup-redis() {
  systemctl enable redis
  systemctl start redis
}

function setup-docker() {
  systemctl enable docker
  systemctl start docker
}

function setup-application-directory() {
  mkdir /var/www
  chown -R $USERNAME /var/www
  # clone application
  # run bundle install
  # chmod -R 775 $USERNAME $APPNAME
}

run-basic-configuration
install-basic-packages
create-vm-user $USERNAME
install-ruby
setup-git
setup-postgres
create-postgres-user
setup-nginx
setup-application-directory

cat > /etc/nginx/nginx.conf <<EOF
#user html;
worker_processes  1; # this may connect with the worker numbers puma can use.
error_log  logs/error.log;
pid        logs/nginx.pid;

events {
    worker_connections  1024;
}

http {
  upstream app {
      # Path to Puma SOCK file, as defined previously
      server unix:/var/www/$APPNAME/shared/sockets/puma.sock;
  }
  server {
      listen 80;
      server_name localhost;
      root /var/www/$APPNAME/public;
      try_files $uri/index.html $uri @app;
      location @app {
        proxy_pass http://app;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $http_host;
        proxy_redirect off;
      }
      error_page 500 502 503 504 /500.html;
      client_max_body_size 4G;
      keepalive_timeout 10;
  }
}
EOF

cat > $HOME/.bundle/config <<EOF
---
BUNDLE_PATH: ".bundle"

EOF

cat > $HOME/.gemrc <<EOF
gem: --no-ri --no-rdoc
install: --no-ri --no-rdoc
update: --no-ri --no-rdoc

EOF

cat > $HOME/.railsrc <<EOF
--skip-bundle
--skip-spring
--skip-test-unit
--database postgresql

EOF


