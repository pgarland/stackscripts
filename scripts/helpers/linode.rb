class Linode

  STATUSES = {
    -1 => 'building',
    0 => 'new',
    1 => 'running',
    2 => 'off'
  }
  DISTRO = 142
  STACKSCRIPT = 14130
  DISK_SIZE = 24064
  SWAP_SIZE = 512

  class << self

    def list_commands
      data = LinodeApi.request('api.spec', &:get)
      data['DATA']
    end

    def images
      data = LinodeApi.request('image.list', &:get)
      data['DATA']
    end

    def distributions
      data = LinodeApi.request('avail.distributions', &:get)
      data['DATA']
    end

    def kernels
      data = LinodeApi.request('avail.distributions', &:get)
      data['DATA']
    end

    def all
      data = LinodeApi.request('linode.list') { |conn| conn.get }
      data['DATA'].map { |node| Linode.new(node) }
    end

    def destroy_all
      all.each { |node|
        next if node.status == 'off'
        node.destroy
      }
    end

    def new_from_stackscript(script_id = STACKSCRIPT)
      data = create_new_instance
      linode = Linode.new(data)
      linode.rebuild_from_stackscript(script_id)
    end

    def create_new_instance
      params = {
        planid: 1,
        payment_term: 24,
        datacenterid: 6
      }
      result = LinodeApi.request('linode.create', params, &:post)
      result['DATA']
    end

  end

  def initialize(data = {})
    @id = data['LINODEID'] || data['LinodeID']
    @status = data['STATUS'] || status
  end

  def list_disks
    data = LinodeApi.request('linode.disk.list', default_params, &:get)
    data['DATA'].map { |o| { o['DISKID'] => o['STATUS'] } }
  end

  def delete_disks
    jobs = []
    list_disks.each do |disk|
      jobs << {
        api_action: 'linode.disk.delete',
        linodeid: @id,
        diskid: disk.keys.first
      }
    end
    return if jobs.empty?
    LinodeApi.batch(jobs)
    @disks = nil
  end

  def destroy
    shutdown if status != 'off'
    delete_disks
    LinodeApi.request('linode.delete', default_params, &:post)
  end

  def shutdown
    LinodeApi.request('linode.shutdown', default_params, &:post)
  end

  def boot
    LinodeApi.request('linode.boot', default_params, &:get)
  end

  def jobs
    LinodeApi.request('linode.job.list', default_params, &:get)
  end

  def status
    data = LinodeApi.request('linode.list', default_params, &:get)
    result = data['DATA'].map { |s|
      @status = s['STATUS'] if s['LINODEID'] == @id
      STATUSES[s['STATUS']]
    }
    result[0]
  end

  def add_swap_disk
    params = {
      linodeid: @id,
      label: 'swap-disk',
      size: SWAP_SIZE,
      type: 'swap'
    }
    LinodeApi.request('linode.disk.create', params, &:post)
  end

  def rebuild_from_stackscript(script_id = STACKSCRIPT)
    delete_disks
    params = {
      stackscriptudfresponses: {}.to_json,
      distributionid: DISTRO,
      linodeid: @id,
      stackscriptid: script_id,
      label: 'Arch-Ruby-Server',
      rootpass: '8p8ypoy',
      size: DISK_SIZE
    }
    LinodeApi.request('linode.disk.createfromstackscript', params, &:post)
    add_swap_disk
    binding.pry
    boot
  end

  private

  def default_params
    {
      linodeid: @id
    }
  end

end
