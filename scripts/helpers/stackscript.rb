class StackScript
  class << self
    STACKSCRIPT = 14130
    DISTRO = 142

    def all(params = {})
      data = LinodeApi.request('stackscript.list', {}) { |conn| conn.get }
      data['DATA']
    end

    def current_dir
      FileUtils.pwd
    end

    def update(script_id = STACKSCRIPT)
      params = {
        rev_note: 'updated via api',
        distribution_id_list: DISTRO,
        label: 'Ruby-Server-V3',
        stackscriptid: script_id,
        is_public: 0
      }
      LinodeApi.request('stackscript.update', params) { |conn|
        conn.post do |req|
          req.headers['Content-Type'] = 'application/json'
          req.body = {
            script: File.read("#{current_dir}/stack.rb")
          }.to_json
        end
      }
    end

  end
end
