class LinodeApi
  class << self

    def batch(jobs)
      params = { api_requestArray: jobs.to_json }
      request('batch', params) { |c| c.post }
    end

    def request(action, params = {})
      puts params
      conn = Faraday.new(
        url: 'https://api.linode.com',
        params: {
          api_key: API_KEY,
          api_action: action
        }.merge(params)
      )
      response = yield(conn)
      result = JSON.parse(response.body)
      puts result
      return result
    end

  end
end
