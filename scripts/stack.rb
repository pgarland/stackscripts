#!/bin/ruby
require 'open3'
require 'fileutils'

HOSTNAME = 'linode'
USERNAME = 'deployer'
USERPASS = 'bearcreek'
GITNAME = 'Peter Garland'
GITEMAIL = 'pngarland@gmail.com'
APPNAME = 'my_rails_app'
HOME = format('/home/%s', USERNAME)
PROFILE_PATH = format('%s/.bash_profile', HOME)
DEVMACHINE = ENV['LOGNAME'] == 'pgarla0'
if DEVMACHINE
  require 'pry'
  LOGFILE = '/Users/pgarla0/.stackscript-logfile'
else
  LOGFILE = '/opt/.log'
end
system("touch #{LOGFILE}")

def log(msg, body = nil)
  puts ">>>>>>>>>>>>>>>> #{msg}"
  puts body if body
end

def run(cmd)
  execute_command('', cmd)
end

def run_as(user, cmd)
  execute_command("su - #{user} -c ", cmd)
end

def write_to_log(data, status)
  return if data.empty?
  open(LOGFILE, 'a') { |f|
    msg = [status, data].join(' >> ')
    f.write(msg)
  }
end

def execute_command(prepend, cmd)
  log 'RUNNING COMMAND....', cmd
  output, err, s = Open3.capture3 cmd.gsub(/^\s{4}/, prepend)
  write_to_log(output, 'INFO')
  write_to_log(err, 'ERROR')
  log 'STATUS:', s
end

def basic_configuration
  run <<-END
    echo #{HOSTNAME} > /etc/hostname
    hostname -F /etc/hostname

    ## Set base locale
    LOCALE_BASE=$(echo $LOCALE | cut -d. -f1)

    ## Get a sensible locale (in case the selected one doesn't exist)
    grep "^${LOCALE}" /etc/locale.gen &>/dev/null
    (( $? > 0 )) && LOCALE=${LOCALE_BASE}
    grep "^${LOCALE}" /etc/locale.gen &>/dev/null
    if (( $? > 0 ))
    then
      LOCALE="en_US.UTF-8"
      LOCALE_BASE="en_US"
    fi
    echo "LANG=$LOCALE" > /etc/locale.conf

    ## Set timezone to sensible value (in case the selected one doesn't exist)
    grep "${TIMEZONE}" /usr/share/zoneinfo/zone.tab &>/dev/null
    (( $? > 0 )) && TIMEZONE="America/Chicago"

    # user config
    echo "%wheel ALL=(ALL) ALL" >> /etc/sudoers
  END
end

def install_packages
  run <<-END
    pacman -Syu --noconfirm
    pacman -S --noconfirm git nginx postgresql vim base-devel
  END
end

def create_user
  run <<-END
    useradd -m -g users -G wheel -s /bin/bash #{USERNAME}
    passwd #{USERNAME} <<EOF
    #{USERPASS}
    #{USERPASS}
    EOF
  END
end

def install_ruby
  run USERNAME, <<-END
    git clone 'https://github.com/sstephenson/rbenv.git' #{HOME}/.rbenv
    git clone 'https://github.com/sstephenson/ruby-build.git' #{HOME}/.rbenv/plugins/ruby-build
    echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> #{PROFILE_PATH}
    echo 'eval "$(rbenv init -)"' >> #{PROFILE_PATH}
    source #{PROFILE_PATH}
    rbenv install 2.2.3
    rbenv global 2.2.3
  END
end

def setup_git
  run_as USERNAME, <<-END
    ssh-keygen -t rsa -f #{HOME}/.ssh/id_rsa -N #{USERPASS}
    git config --global user.name #{GITNAME}
    git config --global user.email #{GITEMAIL}
  END
end

def build
  basic_configuration
  install_packages
  create_user
  install_ruby
rescue => e
  log 'ERROR:', e.message
end

def test_method
  run 'cat ~/.aliases'
  run <<-END
    git clone 'https://github.com/sstephenson/rbenv.git' ~/.rbenv
  END
  run_as 'postgres', <<-END
    ls -al
  END
  create_user
end

if DEVMACHINE
  test_method
else
  build
end
